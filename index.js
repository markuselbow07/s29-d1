// import expre module and save it in a constant
const express = require("express");

// Create an application using express
const app = express();

// App server will listen to port 3000
const port = 3000;

// Setup ffor allowing the server to handle data from request
// Allows the pp to read json data
app.use(express.json());

// Allows the app to read data from forms
app.use(express.urlencoded({extended:true}));

// GET route

app.get("/", (req, res) => {
	res.send("Hello World");
})

app.get("/hello", (req, res) => {
	res.send("Hello from the /hello endpoint!");
})
// POST Route (create)

app.post("/hello", (req, res) => {
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`);
})
// Mock database

let users = [];

// Will create a signup post
app.post("/signup", (req, res) => {
	console.log(req.body);

	if(req.body.username !== '' && req.body.password !== ''){
		users.push(req.body);
		res.send(`User ${req.body.username} successfully registered`);		
	}
	else{
		res.send("Please input BOTH username and password");
	}
})


// PUT request for changing the password
app.put("/change-password", (req, res) => {
	// Creates a variable to store the message to be sent back to the client/Postman
	let message;

	// Creates a for loop that will loop through the elements of the "users" array
	for(let i = 0; i < users.length; i++){

		if(req.body.username === users[i].username){

			users[i].password = req.body.password;

			message = `User ${req.body.username}'s password has been updated.`;
			break;
		}
		else{
			message = "User does not exist"
		}
	}

	res.send(message);
})

// Activity 1

app.get("/ ", (req, res) => {
	res.send("Hello! Nice to see you folk");
})

// 2
app.get("/home", (req, res) => {
	res.send("Hello /nice to see you again!");
})

// 3,4


app.get("/users", (req, res) => {
	console.log(req.body);

	if(req.body.username !== '' && req.body.password !== ''){
		users.push(req.body);
		res.send(`User ${req.body.username} retrieve from the database`);		
	}
	else{
		res.send("Please input BOTH username and password");
	}
})

//  5,6


app.delete("/delete-user", (req, res) => {
	console.log(req.body);

	if(req.body.username !== '' && req.body.password !== ''){
		users.push(req.body);
		res.send(`User ${req.body.username} deleted from the database`);		
	}
	else{
		res.send("Please input BOTH username and password");
	}
})


// Tells the server to listen to the port
app.listen(port, () => console.log(`Server is running at port ${port}`));

